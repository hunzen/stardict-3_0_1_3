/* 
 * This file part of StarDict - A international dictionary for GNOME.
 * http://stardict-4.sourceforge.net
 * Copyright (C) 2005-2006 Evgeniy <dushistov@mail.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <cstring>

#include <glib.h>
#include <glib/gi18n.h>
#include <cstdlib>
#include <gtk/gtk.h>

#ifdef CONFIG_GNOME
#  include <libgnome/libgnome.h>
#  include <libgnomeui/libgnomeui.h>
#elif defined(_WIN32)
#  include <gdk/gdkwin32.h>
#endif

#include "utils.h"


void ProcessGtkEvent()
{
  while (gtk_events_pending())
    gtk_main_iteration();
}

std::string get_user_config_dir()
{
        const gchar *config_path_from_env = g_getenv("STARDICT_CONFIG_PATH");
        if (config_path_from_env)
            return config_path_from_env;
#ifdef _WIN32
	std::string res = g_get_user_config_dir();
	res += G_DIR_SEPARATOR_S "StarDict";
	return res;
#else
	std::string res;
	gchar *tmp = g_build_filename(g_get_home_dir(), ".stardict", NULL);
	res=tmp;
	g_free(tmp);
	return res;
#endif
}

std::string combnum2str(gint comb_code)
{
  switch (comb_code) {
#ifdef _WIN32
  case 0:
    return "Shift";
  case 1:
    return "Alt";
  case 2:
    return "Ctrl";
  case 3:
    return "Ctrl+Alt";
#else
  case 0:
    return "Win";
  case 1:
    return "Shift";
  case 2:
    return "Alt";
  case 3:
    return "Ctrl";
  case 4:
    return "Ctrl+Alt";
  case 5:
    return "Ctrl+e";
  case 6:
    return "F1";
  case 7:
    return "F2";
  case 8:
    return "F3";
  case 9:
    return "F4";
#endif
  default:
    return "";
  }
}

std::vector<std::string> split(const std::string& str, char sep)
{
	std::vector<std::string> res;
	std::string::size_type prev_pos=0, pos = 0;
	while ((pos=str.find(sep, prev_pos))!=std::string::npos) {
		res.push_back(std::string(str, prev_pos, pos-prev_pos));
		prev_pos=pos+1;
	}
	res.push_back(std::string(str, prev_pos, str.length()-prev_pos));

	return res;
}

GdkPixbuf *load_image_from_file(const std::string& filename)
{
	GError *err=NULL;
	GdkPixbuf *res=gdk_pixbuf_new_from_file(filename.c_str(), &err);
	if (!res) {		
		 GtkWidget *message_dlg = 
			 gtk_message_dialog_new(
															NULL,
															(GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
															GTK_MESSAGE_ERROR,
															GTK_BUTTONS_OK,
															_("Can not load image. %s"), err->message);
    
    gtk_dialog_set_default_response(GTK_DIALOG(message_dlg), GTK_RESPONSE_OK);
    
    gtk_window_set_resizable(GTK_WINDOW(message_dlg), FALSE);
    
    gtk_dialog_run(GTK_DIALOG(message_dlg));
    gtk_widget_destroy(message_dlg);
		g_error_free(err);
		exit(EXIT_FAILURE);
	}

	return res;
}

static gchar * byte_to_hex(unsigned char nr) {
	gchar *result = NULL;

	result = g_strdup_printf("%%%x%x", nr / 0x10, nr % 0x10);
	return result;
}

char *common_encode_uri_string(const char *string)
{
	gchar		*newURIString;
	gchar		*hex, *tmp = NULL;
	int		i, j, len, bytes;

	/* the UTF-8 string is casted to ASCII to treat
	   the characters bytewise and convert non-ASCII
	   compatible chars to URI hexcodes */
	newURIString = g_strdup("");
	len = strlen(string);
	for(i = 0; i < len; i++) {
		if(g_ascii_isalnum(string[i]) || strchr("-_.!~*'()", (int)string[i]))
		   	tmp = g_strdup_printf("%s%c", newURIString, string[i]);
		else if(string[i] == ' ')
			tmp = g_strdup_printf("%s%%20", newURIString);
		else if((unsigned char)string[i] <= 127) {
			tmp = g_strdup_printf("%s%s", newURIString, hex = byte_to_hex(string[i]));g_free(hex);
		} else {
			bytes = 0;
			if(((unsigned char)string[i] >= 192) && ((unsigned char)string[i] <= 223))
				bytes = 2;
			else if(((unsigned char)string[i] > 223) && ((unsigned char)string[i] <= 239))
				bytes = 3;
			else if(((unsigned char)string[i] > 239) && ((unsigned char)string[i] <= 247))
				bytes = 4;
			else if(((unsigned char)string[i] > 247) && ((unsigned char)string[i] <= 251))
				bytes = 5;
			else if(((unsigned char)string[i] > 247) && ((unsigned char)string[i] <= 251))
				bytes = 6;
				
			if(0 != bytes) {
				if((i + (bytes - 1)) > len) {
					g_warning(("Unexpected end of character sequence or corrupt UTF-8 encoding! Some characters were dropped!"));
					break;
				}

				for(j=0; j < (bytes - 1); j++) {
					tmp = g_strdup_printf("%s%s", newURIString, hex = byte_to_hex((unsigned char)string[i++]));
					g_free(hex);
					g_free(newURIString);
					newURIString = tmp;
				}
				tmp = g_strdup_printf("%s%s", newURIString, hex = byte_to_hex((unsigned char)string[i]));
				g_free(hex);
			} else {
				/* sh..! */
				g_error("Internal error while converting UTF-8 chars to HTTP URI!");
			}
		}
		g_free(newURIString); 
		newURIString = tmp;
	}
	return newURIString;
}

void common_terminal2pango(const char *t, std::string &pango)
{
	pango.clear();
	const char *p1;
	std::string color_num;
	std::string color;
	std::string tmp_str;
	while (*t) {
		if ((*t == '_') && (t[1] == '')) {
			color = "#FFFA00"; // should be #FFFFFF in fact.
			pango += "<span foreground=\"";
			pango += color;
			pango += "\">";
			t+=2;
			pango += *t;
			pango += "</span>";
			if (*t) {
				t++;
				continue;
			} else {
				break;
			}
		}
		if ((*t == '') && (t[1] == '[')) {
			p1 = strchr(t+2, 'm');
			if (p1) {
				color_num.assign(t+2, p1-t-2);
				if (color_num == "30") {
					color = "#000000";
				} else if (color_num == "31") {
					color = "#FF0000";
				} else if (color_num == "32") {
					color = "#008000";
				} else if (color_num == "33") {
					color = "#FFFF00";
				} else if (color_num == "34") {
					color = "#0000FF";
				} else if (color_num == "35") {
					color = "#FF00FF";
				} else if (color_num == "36") {
					color = "#00FFFF";
				} else if (color_num == "37") {
					color = "#FFFFFF";
				} else {
					color = "#EEEEEE"; // No reason.
				}
				t = p1 +1 ;
				pango += "<span foreground=\"";
				pango += color;
				pango += "\">";
				p1 = strstr(t, "[m");
				if (p1) {
					tmp_str.assign(t, p1-t);
					pango += tmp_str;
					t = p1 +3;
					pango += "</span>";
				} else {
					pango += t;
					pango += "</span>";
					break;
				}
			} else {
				break;
			}
		} else {
			pango += *t;
			t++;
		}
	}
}

void common_terminal2pango_2(const char *t, std::string &pango)
{
	pango.clear();
	std::string prev_str;
	std::string prev_char;
	const char *p1;
	while (*t) {
		if (*t == 8) {
			if (g_str_has_prefix(t+1, prev_char.c_str())) {
				t++;
				pango += "<b>";
				pango += prev_str;
				pango += "</b>";
			} else if (prev_char == "_") {
				t++;
				p1 = g_utf8_next_char(t);
				char *mark = g_markup_escape_text(t, p1 -t);
				pango += "<u>";
				pango += mark;
				pango += "</u>";
				g_free(mark);
			}
			prev_str.clear();
		} else {
			pango += prev_str;
			switch (*t) {
				case '&':
					prev_str = "&amp;";
					break;
				case '<':
					prev_str = "&lt;";
					break;
				case '>':
					prev_str = "&gt;";
					break;
				case '\'':
					prev_str = "&apos;";
					break;
				case '"':
					prev_str = "&quot;";
					break;
				default:
					p1 = g_utf8_next_char(t);
					prev_str.assign(t, p1-t);
					break;
			}
		}
		p1 = g_utf8_next_char(t);
		prev_char.assign(t, p1-t);
		t = p1;
	}
	pango += prev_str;
}

char *common_build_dictdata(char type, const char *definition)
{
	size_t len = strlen(definition);
	guint32 size;
	size = sizeof(char) + len + 1;
	char *data = (char *)g_malloc(sizeof(guint32) + size);
	char *p = data;
	*((guint32 *)p)= size;
	p += sizeof(guint32);
	*p = type;
	p++;
	memcpy(p, definition, len+1);
	return data;
}

