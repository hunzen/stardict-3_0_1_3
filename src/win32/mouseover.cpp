/* 
 * This file part of StarDict - A international dictionary for GNOME.
 * http://stardict-4.sourceforge.net
 *
 * Copyright (C) 2006 Hu Zheng <huzheng_001@hotmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
星际译王(StarDict)的2.4.6版新增加了Windows下屏幕取词的功能。在开发这个功能时参考了Mueller Electronic Dicionary(一个用Delphi编的词典软件)的源代码，以及网上的文档。开发语言是C，用Dev-cpp或Visual Studio编译。
这个功能现在还不太完善，目前有以下问题：
1. 在Win2k系统下，对桌面上的英文取词时为乱码。Windows XP则没有问题。此问题可忽略。
2. 在标题栏，开始菜单及IE, FireFox, Opear等软件上取词时，获取的Y坐标值不正确。见源码包里的src/win32/TextOutHook.c的IsInsidePointW()里的注释。
3. cmd.exe(命令提示符)无法取词。见源码包里的src/win32/GetWord.c的RemoteExecute()里的注释。此问题可忽略。
4. Abode Acrobat Professional无法取词。后来像金山词霸那样编个Abode Acrobat Professional的插件，可以取词了。不过Adobe Reader仍然无法取词，因为没有付钱买插件证书，不过这件事暂时不急。此问题基本已解决。
希望高手能帮忙解决。
*/

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "../stardict.h"

#include "mouseover.h"
#include "ThTypes.h"

// StarDict's Mouseover feature get the example delphi source code from Mueller Electronic Dicionary.
// Homepage: http://vertal1.narod.ru/mueldic.html E-mail: svv_soft@mail.ru

const int WM_MY_SHOW_TRANSLATION = WM_USER + 300;

void Mouseover::NeedSpyDll()
{
	if (fSpyDLL == 0) {
		// Notice, the path must be absolute!
		fSpyDLL = LoadLibrary((gStarDictDataDir+ G_DIR_SEPARATOR_S "TextOutSpy.dll").c_str());
		if (fSpyDLL==0) {
			fSpyDLL = (HINSTANCE)-1;
		} else {
			ActivateSpy_func = (ActivateSpy_func_t)GetProcAddress(fSpyDLL, "ActivateTextOutSpying");
		}
	}
}

HWND Mouseover::Create_hiddenwin()
{
	WNDCLASSEX wcex;
	TCHAR wname[32];

	strcpy(wname, "StarDictMouseover");

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style	        = 0;
	wcex.lpfnWndProc	= (WNDPROC)mouseover_mainmsg_handler;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= stardictexe_hInstance;
	wcex.hIcon		= NULL;
	wcex.hCursor		= NULL,
	wcex.hbrBackground	= NULL;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= wname;
	wcex.hIconSm		= NULL;

	RegisterClassEx(&wcex);

	// Create the window
	return (CreateWindow(wname, "", 0, 0, 0, 0, 0, GetDesktopWindow(), NULL, stardictexe_hInstance, 0));
}

void Mouseover::ShowTranslation()
{
	if (!conf->get_bool_at("dictionary/scan_selection")) // Needed by acrobat plugin.
		return;
	if (conf->get_bool_at("dictionary/only_scan_while_modifier_key")) {
    	bool do_scan = gpAppFrame->unlock_keys->is_pressed();
		if (!do_scan)
			return;
	}
	if (g_utf8_validate(GlobalData->CurMod.MatchedWord, -1, NULL)) {
		gpAppFrame->SmartLookupToFloat(GlobalData->CurMod.MatchedWord, GlobalData->CurMod.BeginPos, true);
	} else {
		char *str1 = g_locale_to_utf8(GlobalData->CurMod.MatchedWord, GlobalData->CurMod.BeginPos, NULL, NULL, NULL);
		if (!str1)
			return;
		char *str2 = g_locale_to_utf8(GlobalData->CurMod.MatchedWord + GlobalData->CurMod.BeginPos, -1, NULL, NULL, NULL);
		if (!str2) {
			g_free(str1);
			return;
		}
		GlobalData->CurMod.BeginPos = strlen(str1);
		char *str = g_strdup_printf("%s%s", str1, str2);
		g_free(str1);
		g_free(str2);
		gpAppFrame->SmartLookupToFloat(str, GlobalData->CurMod.BeginPos, true);
		g_free(str);
	}
}

LRESULT CALLBACK Mouseover::mouseover_mainmsg_handler(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg) {
		case WM_MY_SHOW_TRANSLATION:
			ShowTranslation();
			break;
		default:
			/*nothing*/;
	}

	return DefWindowProc(hwnd, msg, wparam, lparam);
}

Mouseover::Mouseover()
{
	fSpyDLL = 0;
	ActivateSpy_func = NULL;
}

void Mouseover::Init()
{
	ThTypes_Init();
	ZeroMemory(GlobalData, sizeof(TGlobalDLLData));
	strcpy(GlobalData->LibName, (gStarDictDataDir+ G_DIR_SEPARATOR_S "TextOutHook.dll").c_str());
	GlobalData->ServerWND = Create_hiddenwin();
}

void Mouseover::End()
{
	if ((fSpyDLL!=0)&&(fSpyDLL!=(HINSTANCE)-1)) {
		stop();
		FreeLibrary(fSpyDLL);
	}
	DestroyWindow(GlobalData->ServerWND);
	Thtypes_End();
}

void Mouseover::start()
{
	NeedSpyDll();
	if (ActivateSpy_func)
		ActivateSpy_func(true);
}

void Mouseover::stop()
{
	if (ActivateSpy_func)
		ActivateSpy_func(false);
}
