StarDict can be compiled and run in windows.

Please install Dev-C++, it can be found at http://sourceforge.net/projects/orwelldevcpp/
My file: "Dev-Cpp 5.11 TDM-GCC 4.9.2 Setup.exe".

Then install developer packages, they can be found at https://download.gnome.org/binaries/win32/gtk%2B/
Download all-in-one bundle:
gtk+-bundle_2.24.10-20120208_win32.zip
Extract it to C:\Program Files\Dev-Cpp\MinGW64\lib32\

Here is a stardict.dev
Use Dev-C++ to open it, then compile it. Becase the compile command is too long, it can't be compiled in Win98, please use Win2000 or XP.

My environment: Windows XP.

After compiled, you will find stardict.exe at src/.

You can find plug-in projects at stardict-plugins/.

You can find the hook dll projects as src/win32/TextOutHook.dev and src/win32/TextOutSpy.dev.

For the sapi-tts plugin, you need to compile it by vs2005, see mvsc/readme.txt.

For StarDict.api, see: src/win32/acrobat/readme.txt

========
To build the installer.

Grab and install NSIS: http://sourceforge.net/projects/nsis/
My file: nsis-3.07-setup.exe

I use Linux commmand to illustrate the steps, but you cann't do it in this way :)

===============
cd stardict-3.0.1.x
mkdir win32-install-dir

cp src/stardict.exe win32-install-dir
//cp BuildDict.exe win32-install-dir
cp src/win32/TextOutSpy.dll win32-install-dir
cp src/win32/TextOutHook.dll win32-install-dir
cp src/win32/acrobat/win32/StarDict.api win32-install-dir

Find .gmo files in Linux!
mkdir -p win32-install-dir/locale/zh_CN/LC_MESSAGES/
cp po/zh_CN.gmo win32-install-dir/locale/zh_CN/LC_MESSAGES/stardict.mo
mkdir -p win32-install-dir/locale/ru/LC_MESSAGES/
cp po/ru.gmo win32-install-dir/locale/ru/LC_MESSAGES/stardict.mo
mkdir -p win32-install-dir/locale/cs/LC_MESSAGES/
cp po/cs.gmo win32-install-dir/locale/cs/LC_MESSAGES/stardict.mo
And so on!

mkdir -p win32-install-dir/pixmaps
cp pixmaps/stardict.png win32-install-dir/pixmaps
cp src/pixmaps/* win32-install-dir/pixmaps //Notice: docklet_*.png needn't copy!!!
mkdir -p win32-install-dir/sounds
cp src/pixmaps/*.wav win32-install-dir/sounds

mkdir -p win32-install-dir/dic
cp -r stardict-wordnet-3.0.0 win32-install-dir/dic

mkdir -p win32-install-dir/treedict

mkdir -p win32-install-dir/help

You can create stardict.html file by "yelp-pregenerate stardict.xml", yelp-pregenerate is included in yelp-2.4.2, the newer version don't have this tool anymore. Then do some string replacing:
replace "file:///usr/share/sgml/docbook/yelp/docbook/images/" to "../"
replace "ghelp:stardict.xml?" to "./stardict.html#"
replace "ghelp:stardict.xml" to "./stardict.html#id2772190"  //id2772190 is "<b>Table of Contents</b>" 's previous name.
replace "file://./figures/" to "./figures/"
And build the chm files by "Visual CHM".

https://sourceforge.net/projects/stardict-4/files/gtk2-runtime/
Download gtk2-runtime-2.12.1.exe and rename it to gtk-runtime-2.12.1.exe, then put it into gtk_installer/.
You can still look at: https://sourceforge.net/projects/gtk-win/files/GTK%2B Runtime Environment/

Right click the stardict-installer.nsi file, choose "Compile NSIS script" will build the installer.


For debug, use these codes:
=====
FILE *file;
file=fopen("C:\\a.txt", "a+");
fprintf(file, "a\n");
fclose(file);
=====

StarDict's win32 port got many experience from ReciteWord(My another project, http://reciteword.sourceforge.net) and Pidgin: https://www.pidgin.im/development/building/2.x.y/windows/

Note: when use fopen(), use "rb", never "r".

Hu Zheng <huzheng_001@hotmail.com> http://www.huzheng.org
2022.7.20
